﻿using Coscine.Configuration;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeWaterbutlerGitlab.Tests
{
    [TestFixture]
    public class ResourceTypeWaterbutlerGitlabTest
    {
        private IConfiguration _configuration;
        private Dictionary<string, string> _options = new Dictionary<string, string>();
        private ResourceTypeWaterbutlerGitlab _resourceTypeWaterbutlerGitlab;
        private string _repositoryNumber;
        private string _host;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _configuration = new ConsulConfiguration();
            _resourceTypeWaterbutlerGitlab = new ResourceTypeWaterbutlerGitlab("gitlab", _configuration, null);
            _repositoryNumber = "48729";
            _host = "https://git.rwth-aachen.de/";
            _options = new Dictionary<string, string> { { "token", _configuration.GetStringAndWait("coscine/global/gitlabtoken") }, { "repositoryUrl", "coscine/cs/resourcetypewaterbutlergitlab" }, { "repositoryNumber", _repositoryNumber }, { "host", _host } };
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {

        }

        [Test]
        public async Task ListEntriesAsync()
        {
            var result = await _resourceTypeWaterbutlerGitlab.ListEntries(null, "/", _options);
            Assert.True(result.Count > 0);
            Assert.True(result.Where(x => x.Key == "/README.md").First().HasBody);
            result = await _resourceTypeWaterbutlerGitlab.ListEntries(null, "/README.md", _options);
            Assert.True(result.Count > 0);
            Assert.True(result.Where(x => x.Key == "/README.md").First().HasBody);
        }

        [Test]
        public async Task GetEntryAsync()
        {
            var result = await _resourceTypeWaterbutlerGitlab.GetEntry(null, "/README.md", null, _options);
            Assert.True(result.HasBody);
        }
    }
}
