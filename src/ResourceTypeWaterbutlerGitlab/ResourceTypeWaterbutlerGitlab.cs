﻿using Coscine.Configuration;
using Coscine.ResourceTypeBase;
using Coscine.WaterbutlerHelper;
using Coscine.WaterbutlerHelper.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeWaterbutlerGitlab
{
    public class ResourceTypeWaterbutlerGitlab : ResourceTypeDefinition
    {
        private readonly WaterbutlerInterface _waterbutlerInterface;

        public ResourceTypeWaterbutlerGitlab(string name, IConfiguration gConfig, ResourceTypeConfigurationObject resourceTypeConfiguration) : base(name, gConfig, resourceTypeConfiguration)
        {
            _waterbutlerInterface = new WaterbutlerInterface(Configuration, new DataSourceService(new HttpClient()));
        }

        //  Gitlab is readonly
        public override Task DeleteEntry(string id, string key, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public override Task RenameEntry(string id, string keyOld, string keyNew, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public override Task StoreEntry(string id, string key, Stream body, Dictionary<string, string> options = null)
        {
            return Task.CompletedTask;
        }

        public override Task<ResourceTypeInformation> GetResourceTypeInformation()
        {
            var resourceTypeInformation =  base.GetResourceTypeInformation().Result;
            resourceTypeInformation.ResourceContent.ReadOnly = true;
            return Task.FromResult<ResourceTypeInformation>(resourceTypeInformation);
        }

        public override async Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(prefix, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{prefix}\".");
            }

            var entries = new List<ResourceEntry>();

            foreach (var info in infos)
            {
                entries.Add(new ResourceEntry(info.Path, info.IsFile, info.Size == null ? 0 : (long)info.Size, null, null, info.Created == null ? new DateTime() : (DateTime)info.Created, info.Modified == null ? new DateTime() : (DateTime)info.Modified));
            }

            return entries;
        }

        public override async Task<ResourceEntry> GetEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos.First().IsFile)
            {
                throw new Exception("Not a file.");
            }

            return new ResourceEntry(key, infos.First().IsFile, (long)infos.First().Size, null, null, infos.First().Created == null ? new DateTime() : (DateTime)infos.First().Created, infos.First().Modified == null ? new DateTime() : (DateTime)infos.First().Modified);
        }

        public override async Task<Stream> LoadEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);

            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos.First().IsFile)
            {
                throw new Exception("Not a file.");
            }

            // Do not dispose the response!
            // The stream is later accessed by the MVC, to deliver the file.
            // When disposed, the stream becomes invalid and can't be read.
            var response = await _waterbutlerInterface.DownloadObjectAsync(infos.First(), authHeader);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStreamAsync();
            }

            return null;
        }

        protected void HandleResponse(HttpResponseMessage httpResponseMessage)
        {
            var statuscode = (int)httpResponseMessage.StatusCode;
            if (statuscode == 403)
            {
                throw new RTDForbiddenException();
            }
            else if (statuscode == 404)
            {
                throw new RTDNotFoundException();
            }
            else if (statuscode >= 400 && statuscode < 600)
            {
                throw new RTDBadRequestException();
            }
        }
    }
}
